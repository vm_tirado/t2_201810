package model.estructures.test;


import junit.framework.TestCase;
import model.data_structures.List;
import model.data_structures.Node;

public class ListaTest extends TestCase {
	private List lista;

	private int numeroElementos;


	/**
	 * Construye una nueva lista vacia de Integers
	 */
	public void setupEscenario1( )
	{
		lista = new List( );
		numeroElementos = 100;

	}

	/**
	 * Construye una nueva lista con 500 elementos inicialados con el valor de la posicion por 2
	 */
	public void setupEscenario2( )
	{
		numeroElementos = 500;
		lista = new List( );

		for( int cont = 0; cont < numeroElementos; cont++ )
		{
			lista.add(( new Integer( cont * 2 ) ));
		}

	}

	/**
	 * Construye una nueva lista con 100 elementos inicialados con el valor de la posicion por -1
	 */
	public void setupEscenario3( )
	{
		numeroElementos = 100;
		lista = new List( );

		for( int cont = 0; cont < numeroElementos; cont++ )
		{
			lista.add( new Long( cont * ( -1 ) ) );
		}

	}


	/**
	 * Prueba que los elementos se est�n ingresando correctamente a partir de la cabeza
	 */
	public void testaddFirst( )
	{
		setupEscenario1( );

		for( int cont = 0; cont < numeroElementos; cont++ )
		{
			lista.add( new Integer( 5 * cont ) );

		}

		// Verificar que la lista tenga la longitud correcta
		assertEquals( "No se adicionaron todos los elementos", numeroElementos, lista.size( ) );


	}

	/**
	 * Prueba que los elementos se est�n ingresando correctamente a partir de la cola
	 */
	public void testaddLast( )
	{
		setupEscenario1( );

		for( int cont = 0; cont < numeroElementos; cont++ )
		{
			lista.add( new Integer( 5 * cont ) );

		}

		// Verificar que la lista tenga la longitud correcta
		assertEquals( "No se adicionaron todos los elementos", numeroElementos, lista.size( ) );


	}




	/**
	 * Prueba que se retorne correctamente el primer elemento de la lista
	 */
	public void testDarPrimero( )
	{
		setupEscenario2( );

		Node nodo = lista.getFirst();

		Integer elemento = ( Integer )nodo.getObject();

		// Verificar que el primero elemento sea el correcto
		assertEquals( "El primer elemento no es correcto", 0, elemento.intValue( ) );

	}

}


