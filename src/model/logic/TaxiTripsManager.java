package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.List;


public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private List<Taxi> listaTaxi;
	private List <Service> listaS;
	private ArrayList<String> id;
	boolean estado=false;




	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with " + serviceFile);	
		long startTime = System.nanoTime();

		// TODO Auto-generated method stub

		listaS = new List<Service>();
		listaTaxi = new List<Taxi>();
		id = new ArrayList();


		if (!estado)
		{
		
		try
		{
			FileReader lector = new FileReader(serviceFile);
			FileReader lector2= new FileReader(serviceFile);
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
			Service[] item= gson.fromJson(lector, Service[].class);
			Taxi[] taxi= gson.fromJson(lector2, Taxi[].class);

				for(int i=0; i<item.length;i++)
				{

					listaS.add(item[i]);

				}


				for(int i=0; i<taxi.length;i++)
				{
					
					if(!id.contains(taxi[i].getTaxiId()))
					{
					System.out.println(taxi[i].getTaxiId());	
						System.out.println("Entro ");
						listaTaxi.add(taxi[i]);
						id.add(taxi[i].getTaxiId());
					}


				}


				estado=true;
				lector.close();
				lector2.close();
			}

			catch (Exception e)
			{
				e.printStackTrace();
				System.out.println("Hubo un error al cargar el archivo");
			}


			System.out.println(listaS.size());
			System.out.println(listaTaxi.size()+" Taxi");
			System.out.println("Inside loadServices with " + serviceFile);
		}

		else

		{
			System.err.println("El archivo solamente puede ser cargado una vez en el programa");


		}

	}


	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		System.out.println("Inside getTaxisOfCompany with " + company);
		LinkedList<Taxi> listaT=new List<Taxi>();
		int cont=0;
		for(Taxi tax : listaTaxi)
		{
			if(tax!=null&&tax.getCompany()!=null && tax.getCompany().equalsIgnoreCase(company))
			{
				System.out.println(tax.getTaxiId());
				cont++;
				id.add(tax.getTaxiId());
			}
		}
		System.out.println("Se encontraron" + cont+ " elementos");
		return listaT;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		LinkedList<Service> lista_dropoff = new List<>();
		System.out.println("Inside getTaxisOfCompany with the area" + communityArea);

		for(Service item: listaS  )
		{
			
			if(item!=null&& item.getDropoffComunityArea()==communityArea)
			{
			
				lista_dropoff.add(item);
			
			}
		}
		
		
		System.out.println(lista_dropoff.size());
		return lista_dropoff;
	}
	}



